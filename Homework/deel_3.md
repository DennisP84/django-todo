### Deel 3: Admin page

Django komt automatisch met een admin page. Daar kun je o.a. dingen die in de database zitten bekijken en wijzigen.

Laten we hier eens kijken of opdracht 2 (het aanpassen van een tabel) gelukt is!

1. Draai de django-server (runserver) en ga naar localhost:8000/admin.
2. Je zult zien dat je moet inloggen, maar je hebt nog helemaal geen account o.i.d. De admin pagina is voor admins; en die rechten moet je eerst voor jezelf maken!
3. Stop de server. Voer het commando `python manage.py createsuperuser` uit, en de stappen die daarna komen.
4. Start de server. Ga weer naar de admin pagina. Nu kun je inloggen en zien wat er in de database zit. Kun je zien of de door jou toegevoegde kolom er bij staat?

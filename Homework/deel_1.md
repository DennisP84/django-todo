### Deel 1: logging uitproberen
In views.py staat een functie *delete()*. Die wordt aangeroepen als je een todo-item wilt verwijderen.
1. Probeer de code te begrijpen. Leg hieronder (zo goed als je kunt) uit hoe stukjes code in de bestanden urls.py, views.py, home.html en models.py te maken hebben met het deleten van een todo-item.

[Antwoord]:

2. Probeer er voor te zorgen dat er wat wordt geprint naar de console als iemand een todo-item wil deleten. Gebruik daarvoor gewoon print-statements.

3. Gebruik de al geconfigureerde logger ('logger-example') om wat te printen naar /logs/debug.log als iemand een todo-item wil deleten.
Check of er ook iets in die logs terecht komt!
   
4. Misschien vind je dat printen naar een file maar lastig. Zorg er voor dat de logger 'logger-example' nu ook naar de console gaat printen (zie settings.py)

5. Er komen wel onnodige logs in de console terecht, vooral van django-server. Pas het log-level daarvan aan, zodat hij alleen de belangrijke dingen gaat loggen.
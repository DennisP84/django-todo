### Deel X: App uitbreiden

Voor deze opdracht mag je zelf wat cools bedenken. Ik heb als inspiratie hieronder een voorbeeld gegeven wat je kunt doen; dat zou een mooie oefening zijn.
Deze opdracht mag je in je eigen tijd uitvoeren. Volgende week kun je weer een demo geven van wat je hebt gemaakt!


#### Inspiratie
We gaan de database weer veranderen. Voeg een nieuwe tabel (dat is iets anders dan een kolom!) toe.

De tabel gaat Plants heten, en houdt wat informatie over jouw planten bij.

De tabel krijgt minstens twee velden: de naam van de plant (character), en wanneer je hem het laatst water hebt gegeven (datetime).
Misschien is het ook leuk om aan te geven hoe vaak hij water nodig heeft, en daar iets mee te doen.

Zorg er voor dat je deze tabel op de admin pagina te zien krijgt. Hints: migraties en admin.py

Maak in de header van de app een invulplek waar je planten kunt aanmaken.

Laat je planten ook in de app zien, onder de todo-items. 

Zorg dat je als gebruiker van de app kunt aangeven dat je een plant water hebt gegeven, en update dan de juiste velden in de database.

Als je erg veel tijd over hebt (wat meer inspiratie):
- Laat met een kleur zien dat een plant onderhand wel water nodig heeft.
- Wellicht heb je planten vermoord; geef in je database aan dat de plant dood kan, en zorg dat je dat in de app kunt aangeven.
- Je zou ook een reden kunnen opgeven van de dood v.d. plant.
- etc.
